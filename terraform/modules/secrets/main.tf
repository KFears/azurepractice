resource "kubernetes_secret_v1" "vault-token" {
  metadata {
    name = "vault-token"
  }

  data = {
    "token" = var.vault-token
  }
}

resource "kubernetes_secret_v1" "vault" {
  metadata {
    name = "vault"
  }

  data = {
    "VAULT_SEAL_TYPE"                = "azurekeyvault"
    "AZURE_TENANT_ID"                = var.azure_tenant_id
    "AZURE_CLIENT_ID"                = var.azure_client_id
    "AZURE_CLIENT_SECRET"            = var.azure_client_secret
    "VAULT_AZUREKEYVAULT_VAULT_NAME" = var.vault_azurekeyvault_vault_name
    "VAULT_AZUREKEYVAULT_KEY_NAME"   = var.vault_azurekeyvault_key_name
  }
}
