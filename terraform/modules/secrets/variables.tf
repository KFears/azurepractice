variable "runner-registration-token" {
  type      = string
  default   = ""
  sensitive = true
}

variable "runner-token" {
  type      = string
  default   = ""
  sensitive = true
}

variable "vault-token" {
  type      = string
  default   = ""
  sensitive = true
}

variable "azure_tenant_id" {
  type      = string
  default   = ""
  sensitive = true
}

variable "azure_client_id" {
  type      = string
  default   = ""
  sensitive = true
}

variable "azure_client_secret" {
  type      = string
  default   = ""
  sensitive = true
}

variable "vault_azurekeyvault_vault_name" {
  type      = string
  default   = ""
  sensitive = true
}

variable "vault_azurekeyvault_key_name" {
  type      = string
  default   = ""
  sensitive = true
}
