locals {
  repo_root = "${path.module}/../../.."
}

variable "argocdServerAdminPassword" {
  type      = string
  sensitive = true
}
