resource "helm_release" "argo-cd" {
  name             = "argo-cd"
  chart            = "${local.repo_root}/helm-charts/argo-cd"
  namespace        = "argocd"
  create_namespace = true

  set_sensitive {
    name  = "configs.secret.argocdServerAdminPassword"
    value = var.argocdServerAdminPassword
  }

  lifecycle {
    ignore_changes = all
  }
}

resource "helm_release" "vault" {
  name  = "vault"
  chart = "${local.repo_root}/helm-charts/vault"

  lifecycle {
    ignore_changes = all
  }
}

resource "helm_release" "external-secrets" {
  name  = "external-secrets"
  chart = "${local.repo_root}/helm-charts/external-secrets"

  lifecycle {
    ignore_changes = all
  }
}
