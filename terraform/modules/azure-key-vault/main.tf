resource "random_string" "keyvault" {
  length  = 5
  special = false
  upper   = false
}

data "azurerm_client_config" "current" {}

locals {
  tenant_id = sensitive(data.azurerm_client_config.current.tenant_id)
  object_id = sensitive(data.azurerm_client_config.current.object_id)
}

resource "azuread_application" "vault" {
  display_name = "${var.environment}-vault"
}

resource "azuread_application_password" "vault" {
  application_object_id = azuread_application.vault.object_id
}

resource "azuread_service_principal" "vault" {
  application_id = azuread_application.vault.application_id
}


resource "azurerm_key_vault" "vault" {
  name                = "${var.environment}-vault-${random_string.keyvault.result}"
  location            = var.location
  resource_group_name = var.resource_group_name
  tenant_id           = local.tenant_id

  enabled_for_deployment = true # allow k8s cluster to access vault

  sku_name = "standard"


  # TODO does this really need to be so broad? can it be limited to the vault vm?
  network_acls {
    default_action = "Allow"
    bypass         = "AzureServices"
  }
}

resource "azurerm_key_vault_key" "vault" {
  name         = "hashicorp-vault"
  key_vault_id = azurerm_key_vault.vault.id
  key_type     = "RSA"
  key_size     = 2048

  key_opts = [
    "wrapKey",
    "unwrapKey",
  ]
}
