output "azure_tenant_id" {
  value     = azuread_service_principal.vault.application_tenant_id
  sensitive = true
}

output "azure_client_id" {
  value     = azuread_application.vault.application_id
  sensitive = true
}

output "azure_client_secret" {
  value     = azuread_application_password.vault.value
  sensitive = true
}

output "vault_azurekeyvault_vault_name" {
  value     = azurerm_key_vault.vault.name
  sensitive = true
}

output "vault_azurekeyvault_key_name" {
  value     = azurerm_key_vault_key.vault.name
  sensitive = true
}
