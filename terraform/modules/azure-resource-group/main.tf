resource "azurerm_resource_group" "resource_group" {
  name     = var.environment
  location = var.location
}
