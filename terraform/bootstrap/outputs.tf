output "TEMPLATE_RESOURCE_GROUP_NAME" {
  value = azurerm_resource_group.tfstate.name
}

output "TEMPLATE_STORAGE_ACCOUNT_NAME" {
  value = azurerm_storage_account.tfstate.name
}
