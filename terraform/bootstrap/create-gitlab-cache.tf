resource "random_string" "gitlab_resource_code" {
  length  = 5
  special = false
  upper   = false
}

resource "azurerm_resource_group" "gitlabcache" {
  name     = "gitlabcache"
  location = var.location
}

resource "azurerm_storage_account" "gitlabcache" {
  name                     = "gitlabcache${random_string.gitlab_resource_code.result}"
  resource_group_name      = azurerm_resource_group.gitlabcache.name
  location                 = azurerm_resource_group.gitlabcache.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "gitlabcache" {
  name                  = "gitlabcache"
  storage_account_name  = azurerm_storage_account.gitlabcache.name
  container_access_type = "blob"
}
