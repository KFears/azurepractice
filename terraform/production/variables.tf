variable "location" {
  type = string
}

variable "environment" {
  type = string
}

variable "dns_prefix" {
  type = string
}

variable "argocdServerAdminPassword" {
  type = string
}

variable "vault-token" {
  type      = string
  sensitive = true
}
