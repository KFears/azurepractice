module "azure-resource-group" {
  source = "../modules/azure-resource-group"

  environment = var.environment
  location    = var.location
}

module "azure-k8s-cluster" {
  source = "../modules/azure-k8s-cluster"

  resource_group_name = module.azure-resource-group.name
  location            = module.azure-resource-group.location
  environment         = var.environment
  dns_prefix          = var.dns_prefix
}

module "azure-key-vault" {
  source = "../modules/azure-key-vault"

  resource_group_name = module.azure-resource-group.name
  location            = module.azure-resource-group.location
  environment         = var.environment
}

module "helm-charts" {
  source = "../modules/helm-charts"

  argocdServerAdminPassword = var.argocdServerAdminPassword
}

module "secrets" {
  source = "../modules/secrets"

  vault-token                    = var.vault-token
  azure_tenant_id                = module.azure-key-vault.azure_tenant_id
  azure_client_id                = module.azure-key-vault.azure_client_id
  azure_client_secret            = module.azure-key-vault.azure_client_secret
  vault_azurekeyvault_vault_name = module.azure-key-vault.vault_azurekeyvault_vault_name
  vault_azurekeyvault_key_name   = module.azure-key-vault.vault_azurekeyvault_key_name
}
