resource "azurerm_storage_container" "production" {
  name                  = "production"
  storage_account_name  = "tfstatewv0yu"
  container_access_type = "blob"

  lifecycle {
    prevent_destroy = true
  }
}
