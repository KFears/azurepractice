location                  = "West Europe"
environment               = "production"
dns_prefix                = "roflops-prod"
argocdServerAdminPassword = "$2a$10$evIQu5iWy9Ml4VJSFUU6SuDmaeaXblFv.cHdRJn093vdSwkFu9X8G"
