terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.14.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "=2.27.0"
    }
  }
}

provider "azurerm" {
  features {}
}

provider "azuread" {}

provider "kubernetes" {
  host                   = module.azure-k8s-cluster.host
  username               = module.azure-k8s-cluster.username
  password               = module.azure-k8s-cluster.password
  client_certificate     = base64decode(module.azure-k8s-cluster.client_certificate)
  client_key             = base64decode(module.azure-k8s-cluster.client_key)
  cluster_ca_certificate = base64decode(module.azure-k8s-cluster.cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = module.azure-k8s-cluster.host
    username               = module.azure-k8s-cluster.username
    password               = module.azure-k8s-cluster.password
    client_certificate     = base64decode(module.azure-k8s-cluster.client_certificate)
    client_key             = base64decode(module.azure-k8s-cluster.client_key)
    cluster_ca_certificate = base64decode(module.azure-k8s-cluster.cluster_ca_certificate)
  }
}

