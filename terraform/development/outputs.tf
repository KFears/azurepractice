output "kube_config" {
  value     = module.azure-k8s-cluster.kube_config
  sensitive = true
}
