resource "azurerm_storage_container" "development" {
  name                  = "development"
  storage_account_name  = "tfstatewv0yu"
  container_access_type = "blob"

  lifecycle {
    prevent_destroy = true
  }
}
