location                  = "West Europe"
environment               = "development"
dns_prefix                = "roflops-dev"
argocdServerAdminPassword = "$2a$10$Ds.jtR87QeZcivP3KGG.pO1K9CDsM9.Wo9bAXjh9a7Sq2GUZO8T5S"
