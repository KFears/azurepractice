terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstatewv0yu"
    container_name       = "development"
    key                  = "development.terraform.state"
  }
}
