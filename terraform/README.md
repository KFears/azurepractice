This folder contains all the Terraform files. Don't touch anything but `modules` folder! In case you need to change the specific environment, please go to the `bootstrap` folder and start from there.
You can change `terraform.tfvars`, but this is also discouraged.
