terraform {
  backend "azurerm" {
    resource_group_name  = "TEMPLATE_RESOURCE_GROUP_NAME"
    storage_account_name = "TEMPLATE_STORAGE_ACCOUNT_NAME"
    container_name       = "TEMPLATE_CONTAINER_NAME"
    key                  = "TEMPLATE_KEY"
  }
}
