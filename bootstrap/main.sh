#!/usr/bin/env bash 
set -euxo pipefail
shopt -s inherit_errexit
IFS=$'\n\t'

BOOTSTRAP_ENV="bootstrap" # The name of the bootstrap deployment. Typically, you'd only want to bootstrap once. Don't touch this unless you know what you're doing.




# Cleanup before Terraform was ran
function cleanup_simple() {
  echo "Cancelled or failed. Cleaning up..."
  rm -rf $TF_PATH
  exit 1
}

# Cleanup after Terrraform was ran
function cleanup_destroy() {
  echo "Cancelled or failed. Cleaning up..."
  terraform -chdir=$TF_PATH destroy -auto-approve 
  rm -rf $TF_PATH
  exit 1
}

# Cleanup after Terraform was ran and state was placed into remote storage
function cleanup_with_state() {
  echo "Cancelled or failed. Cleaning up..."
  rm $TF_PATH/remote-backend.tf 
  terraform -chdir=$TF_PATH init -migrate-state
  terraform -chdir=$TF_PATH destroy -auto-approve 
  rm -rf $TF_PATH
  exit 1
}



function tf_init() {
  terraform -chdir=$TF_PATH init
}

function tf_init_migrate_state() {
  terraform -chdir=$TF_PATH init -migrate-state
}

function tf_plan() {
  terraform -chdir=$TF_PATH plan -out main.tfplan
}

function tf_apply() {
  TF_PLAN=""
  [[ -e $TF_PATH/main.tfplan ]] && TF_PLAN="main.tfplan"
  # Is shell interactive? Yes/No
  tty -s && terraform -chdir=$TF_PATH apply $TF_PLAN || tf_apply_auto_approve $TF_PLAN
}

function tf_apply_auto_approve() {
  terraform -chdir=$TF_PATH apply -auto-approve
}

function tf_apply_plan() {
  terraform -chdir=$TF_PATH apply main.tfplan
}

function tf_output() {
  terraform -chdir=$TF_PATH output $1
}






function create_storage_account() {
  echo "Creating storage account..."

  if ! [[ -e $TF_PATH ]]; then
    mkdir $TF_PATH
    cp create-storage-account/* $TF_PATH
    echo "location = \"$LOCATION\"" > $TF_PATH/terraform.tfvars
    tf_init || cleanup_simple

    # Azure storage accounts have to have a globally unique name;
    # since Terraform won't retry if the name already exists, we do retries manually
    for ((i = 0; i < 10; i++)); do 
      # TODO: abort on task cancellation
      tf_apply_auto_approve && break 
      if [[ $i -eq 9 ]]; then exit 1; fi
    done

    TEMPLATE_RESOURCE_GROUP_NAME=$(tf_output TEMPLATE_RESOURCE_GROUP_NAME)
    TEMPLATE_STORAGE_ACCOUNT_NAME=$(tf_output TEMPLATE_STORAGE_ACCOUNT_NAME)

    echo "TEMPLATE_RESOURCE_GROUP_NAME=$TEMPLATE_RESOURCE_GROUP_NAME" >> ./env
    echo "TEMPLATE_STORAGE_ACCOUNT_NAME=$TEMPLATE_STORAGE_ACCOUNT_NAME" >> ./env
  else 
    echo "The directory at $TF_PATH already exists. This might mean that the bootstrapping has already been done. If that's not the case, please carefully review the contents of the $TF_PATH directory and act accordingly. In case you want to bootstrap another cluster, please edit the BOOTSTRAP_ENV variable in the script manually. Exiting now."
    exit 1
  fi
}



function create_blob() {
  echo "Creating blob..."

  cp create-blob/* $TF_PATH/
  sed -i "s/TEMPLATE_AZURERM_STORAGE_CONTAINER/$DEPLOY_ENV/g" $TF_PATH/create-remote-backend.tf
  sed -i "s/TEMPLATE_STORAGE_ACCOUNT_NAME/$TEMPLATE_STORAGE_ACCOUNT_NAME/g" $TF_PATH/create-remote-backend.tf
  tf_init || cleanup_destroy
  tf_apply || cleanup_destroy
}


# Move Terraform state into Azure blob we've created
function move_state() {
  echo "Moving state into blob..."

  cp move-state/* $TF_PATH

  [[ -z $TEMPLATE_STORAGE_ACCOUNT_NAME ]] && echo "Enter resource group name:" && read TEMPLATE_RESOURCE_GROUP_NAME
  [[ -z $TEMPLATE_RESOURCE_GROUP_NAME ]] && echo "Enter storage account name:" && read TEMPLATE_STORAGE_ACCOUNT_NAME
  TEMPLATE_CONTAINER_NAME="$DEPLOY_ENV"
  TEMPLATE_KEY="$DEPLOY_ENV.terraform.state"

  sed -i "s/TEMPLATE_RESOURCE_GROUP_NAME/$TEMPLATE_RESOURCE_GROUP_NAME/g" $TF_PATH/remote-backend.tf
  sed -i "s/TEMPLATE_STORAGE_ACCOUNT_NAME/$TEMPLATE_STORAGE_ACCOUNT_NAME/g" $TF_PATH/remote-backend.tf
  sed -i "s/TEMPLATE_CONTAINER_NAME/$TEMPLATE_CONTAINER_NAME/g" $TF_PATH/remote-backend.tf
  sed -i "s/TEMPLATE_KEY/$TEMPLATE_KEY/g" $TF_PATH/remote-backend.tf

  tf_init_migrate_state || cleanup_destroy
  if [[ -e $TF_PATH/terraform.tfstate ]]; then rm $TF_PATH/terraform.tfstate; fi
}


# Final stage. This is where we create actual resources that we want in the end
function final() {
  echo "Deploying the environment..."

  cp final/* $TF_PATH
  tf_init || cleanup_with_state
  echo "location = \"$LOCATION\"" > $TF_PATH/terraform.tfvars
  echo "environment = \"$DEPLOY_ENV\"" >> $TF_PATH/terraform.tfvars
  tf_apply || cleanup_with_state
}





# First run. Create Azure storage account, Azure bucket and move Terraform state into it
function bootstrap() {
  echo "Bootstrapping..."

  if [[ "$DEPLOY_ENV" == "$BOOTSTRAP_ENV" ]]; then create_storage_account
  else 
    echo "You are trying to bootstrap an environment with a name that mismatches a BOOTSTRAP_ENV variable (currently: $BOOTSTRAP_ENV). This script assumes you can only bootstrap a single environment, so it keeps the name static. If you want to bootstrap more envionments, please read the script thoroughly and decide for yourself if that's what you really want. Exiting now."
    exit 1
  fi

  if ! [[ -e $TF_PATH/create-remote-backend.tf ]]; then create_blob
  else 
    echo "$TF_PATH/create-remote-backend.tf already exists. bootstrap is supposed to be ran only once, and right after the storage account had been created. If you get this error, chances are, something's gone very wrong. Carefully review what had been done, undo all of it, and run bootstrap again."
  fi

  move_state
}


# All other runs. Create Azure bucket, move Terraform state into it, deploy our actual infra
function deploy() {
  echo "Deploying..."

  if ! [[ -e $TF_PATH ]]; then
    mkdir $TF_PATH
    create_storage_account
    create_blob
    final
  else 
    tf_apply
  fi
}









# Actual script
case $1 in 
  bootstrap)
    source ./env.bootstrap
    TF_PATH="../terraform/$DEPLOY_ENV"

    bootstrap 
    ;;


  deploy)
    [[ -e ./env ]] && source ./env
    [[ -z $DEPLOY_ENV ]] && echo "Enter deployment name, such as \"production\":" && read DEPLOY_ENV
    [[ -z $LOCATION ]] && echo "Enter region, such as \"West Europe\":" && read LOCATION
    TF_PATH="../terraform/$DEPLOY_ENV"

    deploy 
    ;;


  update)
    [[ -e ./env ]] && source ./env
    [[ -z $DEPLOY_ENV ]] && echo "Enter deployment name, such as \"production\":" && read DEPLOY_ENV
    TF_PATH="../terraform/$DEPLOY_ENV"

    cp final/* $TF_PATH
    ;;


  plan)
    [[ -e ./env ]] && source ./env
    [[ -z $DEPLOY_ENV ]] && echo "Enter deployment name, such as \"production\":" && read DEPLOY_ENV
    TF_PATH="../terraform/$DEPLOY_ENV"

    tf_plan
    ;;


  *)
    echo "Usage: $0 {bootstrap|deploy|update}"
    ;;
esac
