resource "azurerm_storage_container" "TEMPLATE_AZURERM_STORAGE_CONTAINER" {
  name                  = "TEMPLATE_AZURERM_STORAGE_CONTAINER"
  storage_account_name  = "TEMPLATE_STORAGE_ACCOUNT_NAME"
  container_access_type = "blob"

  lifecycle {
    prevent_destroy = true
  }
}
