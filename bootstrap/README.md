This script deploys the whole infra from start to finish.
First run: `cp env.base && env ./main.sh bootstrap`
All other runs: fill in the `env` file (optional), then run `./main.sh deploy`


stage 1 creates an Azure storage account, which lets us create Azure blobs
stage 2 creates an Azure blob
stage 3 moves Terraform state from stage 2 into the blob created in stage 2
final deploys our actual infra


`./main.sh bootstrap` runs stages 1, 2 and 3
`./main.sh deploy` runs stages 2, 3 and final

Please do note that you can't run bootstrap more than once (and really shouldn't!), but running deploy more than once is possible, since it's essentially synonymous with running `terraform apply`

This folder also has the `final` folder. It contains all the Terraform code for the final environments. If you need to change the Terraform environments, do it here.
