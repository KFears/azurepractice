FROM ubuntu:focal-20220531


RUN apt-get update && apt-get install -y gnupg software-properties-common curl && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    apt-get update && apt-get install terraform && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash && \
    rm -rf /var/lib/apt/lists/*


ARG ARM_CLIENT_ID
ARG ARM_CLIENT_SECRET 
ARG ARM_SUBSCRIPTION_ID
ARG ARM_TENANT_ID

ENV ARM_CLIENT_ID=$ARM_CLIENT_ID
ENV ARM_CLIENT_SECRET=$ARM_CLIENT_SECRET
ENV ARM_SUBSCRIPTION_ID=$ARM_SUBSCRIPTION_ID
ENV ARM_TENANT_ID=$ARM_TENANT_ID


RUN az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
